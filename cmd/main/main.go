package main

import (
	"fmt"
	"log"
	"os"
	"time-tracker/cmd/router"
	"time-tracker/internal/database"
	"time-tracker/logger"

	"github.com/joho/godotenv"
)

// @title Time Tracker API
// @version 1.0
// @description This is a sample server for a time tracking application.
// @host localhost:8080
// @BasePath /
func main() {
	logger.Init()
	err := godotenv.Load(".env")
	fmt.Println(godotenv.Load(".env"))
	database.ConnectDB()
	port := os.Getenv("APP_PORT")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	r := router.Router()

	logger.Info("Starting server on port 8080")
	go func() {
		if err := r.Run(port); err != nil {
			panic(err)
		}
	}()
	select {}
}
