package task

import (
	"net/http"
	"strconv"
	"time"
	"time-tracker/internal/database"
	"time-tracker/internal/database/structures"
	"time-tracker/logger"

	"github.com/gin-gonic/gin"
)

// @Summary Get tasks of a user
// @Description Get tasks of a user by their ID
// @Produce  json
// @Param id path int true "User ID"
// @Param start query string false "Start date" format(date-time)
// @Param end query string false "End date" format(date-time)
// @Success 200 {array} Task
// @Failure 500 {object} gin.H{"error": "string"}
// @Router /users/{id}/tasks [get]
func GetUserTasks(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	logger.Debug("Received request to get user with ID: ", id)
	var tasks []structures.Task
	if err := database.DB.Where("user_id = ?", id).Order("duration desc").Find(&tasks).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	logger.Info("User task get successfully")
	c.JSON(http.StatusOK, tasks)
}

// @Summary Start a task for a user
// @Description Start a new task for a user by their ID
// @Accept  json
// @Produce  json
// @Param id path int true "User ID"
// @Param task body Task true "Task details"
// @Success 201 {object} Task
// @Failure 400 {object} gin.H{"error": "string"}
// @Router /users/{id}/start-task [post]
func StartTask(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	logger.Debug("Received request to get user with ID: ", id)
	var task structures.Task
	if err := c.ShouldBindJSON(&task); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	task.UserID = uint(id)
	task.StartTime = time.Now()
	database.DB.Create(&task)
	logger.Info("User start task successfully")
	c.JSON(http.StatusCreated, task)
}

// @Summary End a task for a user
// @Description End a running task for a user by their ID
// @Produce  json
// @Param id path int true "User ID"
// @Success 200 {object} Task
// @Failure 404 {object} gin.H{"error": "string"}
// @Router /users/{id}/end-task [post]
func EndTask(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	logger.Debug("Received request to get user with ID: ", id)
	var task structures.Task
	if err := database.DB.Where("user_id = ? AND end_time IS NULL", id).First(&task).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Task not found"})
		return
	}
	task.EndTime = time.Now()
	task.Duration = int(task.EndTime.Sub(task.StartTime).Minutes())
	database.DB.Save(&task)
	logger.Info("User start task successfully")
	c.JSON(http.StatusOK, task)
}
