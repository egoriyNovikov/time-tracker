package user

import (
	"net/http"
	"strconv"
	"time-tracker/internal/database"
	"time-tracker/internal/database/structures"
	"time-tracker/logger"

	"github.com/gin-gonic/gin"
)

// @Summary Create a new user
// @Description Create a new user with the input payload
// @Accept  json
// @Produce  json
// @Param   user      body   User     true   "New User"
// @Success 201 {object} User
// @Failure 400 {object} gin.H{"error": "string"}
// @Router /users [post]
func CreateUser(c *gin.Context) {
	logger.Debug("Received request to create user")
	var newUser structures.User
	if err := c.ShouldBindJSON(&newUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	database.DB.Create(&newUser)
	logger.Info("User created successfully")
	c.JSON(http.StatusCreated, newUser)
}

// @Summary Get list of users
// @Description Get list of users with optional filters and pagination
// @Produce  json
// @Param name query string false "Name"
// @Param email query string false "Email"
// @Param passport_number query string false "Passport Number"
// @Param page query int false "Page number" default(1)
// @Param limit query int false "Number of items per page" default(10)
// @Success 200 {array} User
// @Failure 500 {object} gin.H{"error": "string"}
// @Router /users [get]
func GetUsers(c *gin.Context) {
	logger.Debug("Received request to get users")
	var users []structures.User
	query := database.DB
	if fname := c.Query("fname"); fname != "" {
		query = query.Where("first_name LIKE ?", "%"+fname+"%")
	}
	if lname := c.Query("lname"); lname != "" {
		query = query.Where("last_name LIKE ?", "%"+lname+"%")
	}
	if email := c.Query("email"); email != "" {
		query = query.Where("email LIKE ?", "%"+email+"%")
	}
	if passportNumber := c.Query("passport_number"); passportNumber != "" {
		query = query.Where("passport_number LIKE ?", "%"+passportNumber+"%")
	}
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	limit, _ := strconv.Atoi(c.DefaultQuery("limit", "5"))
	offset := (page - 1) * limit
	if err := query.Offset(offset).Limit(limit).Find(&users).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	logger.Info("Users retrieved successfully")
	c.JSON(http.StatusOK, users)
}

// @Summary Update a user
// @Description Update details of a user by their ID
// @Accept  json
// @Produce  json
// @Param id path int true "User ID"
// @Param user body User true "Updated user details"
// @Success 200 {object} User
// @Failure 400 {object} gin.H{"error": "string"}
// @Failure 404 {object} gin.H{"error": "string"}
// @Router /users/{id} [put]
func UpdateUser(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	logger.Debug("Received request to get user with ID: ", id)
	var user structures.User
	if err := database.DB.First(&user, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	database.DB.Save(&user)

	logger.Info("User retrieved successfully")
	c.JSON(http.StatusOK, user)
}

// @Summary Delete a user
// @Description Delete a user by their ID
// @Produce  json
// @Param id path int true "User ID"
// @Success 200 {object} gin.H{"message": "string"}
// @Failure 404 {object} gin.H{"error": "string"}
// @Router /users/{id} [delete]
func DeleteUser(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	logger.Debug("Received request to get user with ID: ", id)
	var user structures.User
	if err := database.DB.First(&user, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}
	database.DB.Delete(&user)
	logger.Info("User delete successfully")
	c.JSON(http.StatusOK, gin.H{"message": "User deleted"})
}

// @Summary Get a user by ID
// @Description Get details of a user by their ID
// @Produce  json
// @Param id path int true "User ID"
// @Success 200 {object} User
// @Failure 404 {object} gin.H{"error": "string"}
// @Router /users/{id} [get]
func GetUserByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	logger.Debug("Received request to get user with ID: ", id)
	var user structures.User
	if err := database.DB.First(&user, id).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}
	logger.Info("User get successfully")
	c.JSON(http.StatusOK, user)
}
