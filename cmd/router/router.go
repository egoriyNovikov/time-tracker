package router

import (
	"time-tracker/cmd/controller/task"
	"time-tracker/cmd/controller/user"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func Router() *gin.Engine {
	router := gin.Default()

	router.POST("/users", user.CreateUser)
	router.GET("/users", user.GetUsers)
	router.GET("/users/:id", user.GetUserByID)
	router.GET("/users/:id/tasks", task.GetUserTasks)
	router.POST("/users/:id/start-task", task.StartTask)
	router.POST("/users/:id/end-task", task.EndTask)
	router.PUT("/users/:id", user.UpdateUser)
	router.DELETE("/users/:id", user.DeleteUser)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	return router
}
