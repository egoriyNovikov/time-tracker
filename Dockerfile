FROM golang:1.22
WORKDIR /cmd/main
COPY go.mod go.sum ./
RUN go mod download
COPY ./cmd/main/main.go .
RUN go build -o time-tracker ./main.go
CMD ["./time-tracker"]