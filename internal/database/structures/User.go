package structures

import "gorm.io/gorm"

type User struct {
	gorm.Model
	FirstName      string `json:"firstName"`
	LastName       string `json:"lastName"`
	Email          string `json:"email" gorm:"unique"`
	PassportNumber string `json:"passportNumber" gorm:"unique"`
	Tasks          []Task
}
