package structures

import (
	"time"

	"gorm.io/gorm"
)

type Task struct {
	gorm.Model
	UserID    uint      `json:"user_id"`
	Title     string    `json:"title"`
	StartTime time.Time `json:"start_time"`
	EndTime   time.Time `json:"end_time"`
	Duration  int       `json:"duration"` // in minutes
}
