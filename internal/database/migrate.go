package database

import (
	"log"
	"math/rand"
	"time"

	"time-tracker/internal/database/structures"

	"github.com/bxcodec/faker/v3"
)

func AutoMigrate() {
	err := DB.AutoMigrate(&structures.User{}, &structures.Task{})
	var userCount int64
	DB.Model(&structures.User{}).Count(&userCount)
	if userCount == 0 {
		generateFakeUsers(10)
	}
	var taskCount int64
	DB.Model(&structures.Task{}).Count(&taskCount)
	if taskCount == 0 {
		generateFakeTasks(20)
	}
	if err != nil {
		log.Fatalf("Ошибка при миграции схемы: %v", err)
	}
}
func generateFakeUsers(count int) {
	for i := 0; i < count; i++ {
		user := structures.User{
			FirstName:      faker.FirstName(),
			LastName:       faker.LastName(),
			Email:          faker.Email(),
			PassportNumber: faker.Phonenumber(),
		}
		DB.Create(&user)
	}
}
func generateFakeTasks(count int) {
	var users []structures.User
	DB.Find(&users)

	for i := 0; i < count; i++ {
		user := users[i%len(users)]
		startTime := time.Now().Add(-time.Duration(rand.Intn(1000)) * time.Hour)
		endTime := startTime.Add(time.Duration(rand.Intn(8)+1) * time.Hour)
		duration := int(endTime.Sub(startTime).Minutes())

		task := structures.Task{
			UserID:    user.ID,
			Title:     faker.Sentence(),
			StartTime: startTime,
			Duration:  duration,
		}
		DB.Create(&task)
	}
}
